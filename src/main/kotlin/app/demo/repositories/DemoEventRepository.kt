package app.demo.repositories

import app.demo.models.DemoEvent
import app.framework.jdbc.JdbcTemplateRepository
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import javax.sql.DataSource

@Repository
class DemoEventRepository(datasource: DataSource) : JdbcTemplateRepository (datasource) {

    class DemoEventRowMapper : RowMapper<DemoEvent> {
        override fun mapRow(rs: ResultSet, rowNum: Int): DemoEvent? {
            return DemoEvent(
                rs.getInt("id_event"),
                rs.getString("name")
            )
        }
    }

    fun getById(idEvent: Int) : DemoEvent {
        val query =
            """
            select *
            from demo_event de
            where de.id_event = :idDemoEvent
            """
        val demoEvent = jdbcTemplate().queryForObject(query, mapOf("idDemoEvent" to idEvent), DemoEventRowMapper())
        demoEvent?.let {
            return demoEvent
        }
        throw IllegalStateException("Demo Event id $idEvent not found")
    }

}