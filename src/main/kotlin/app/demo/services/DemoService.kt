package app.demo.services

import app.demo.dtos.DemoEventDto
import app.demo.models.DemoPlayer

interface DemoService {

    fun getEventWithPlayers(idEvent:Int) : DemoEventDto

    fun savePlayerInEvent(idEvent:Int, player: DemoPlayer)

    fun deletePlayer(idEvent: Int, idPlayer: Int)
}