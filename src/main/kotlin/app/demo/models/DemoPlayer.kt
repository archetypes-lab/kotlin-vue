package app.demo.models

data class DemoPlayer(
    var idPlayer: Int?,
    var demoEventId: Int,
    var name: String,
    var description: String,
)