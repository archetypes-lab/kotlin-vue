create table demo_event (
	id_event serial,
	name varchar(255),
	primary key (id_event)
);

create table demo_player (
    id_player serial,
    demo_event_id int,
    name varchar(255),
    description varchar(255),
    primary key (id_player),
    foreign key (demo_event_id) references demo_event (id_event)
);