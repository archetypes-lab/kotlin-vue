package app.demo.repositories

import app.demo.models.DemoPlayer
import app.framework.jdbc.JdbcTemplateRepository
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import javax.sql.DataSource

@Repository
class DemoPlayerRepository(datasource: DataSource) : JdbcTemplateRepository(datasource) {

    class DemoPlayerRowMapper : RowMapper<DemoPlayer> {
        override fun mapRow(rs: ResultSet, rowNum: Int): DemoPlayer? {
            return DemoPlayer(
                rs.getInt("id_player"),
                rs.getInt("demo_event_id"),
                rs.getString("name"),
                rs.getString("description")
            )
        }
    }

    fun findByEventId(eventId: Int): List<DemoPlayer> {
        val query =
            """
            select *
            from demo_player dp
            where dp.demo_event_id = :eventId
            """
        return jdbcTemplate().query(query, mapOf("eventId" to eventId), DemoPlayerRowMapper())
    }

    fun update(player: DemoPlayer): Int {
        val query =
            """
            UPDATE demo_player
            SET demo_event_id = :eventId, name = :name, description = :description
            WHERE id_player = :idPlayer
            """
        val params = this.createCommonsParams(player)
        params.addValue("idPlayer", player.idPlayer)
        val keyHolder = GeneratedKeyHolder()
        jdbcTemplate().update(query, params, keyHolder)
        return keyHolder.key!!.toInt()
    }

    fun insert(player: DemoPlayer): Int {
        val query =
            """
            insert into demo_player (demo_event_id, name, description)
            values (:eventId, :name, :description)
            """
        val params = this.createCommonsParams(player)
        val keyHolder = GeneratedKeyHolder()
        jdbcTemplate().update(query, params, keyHolder)
        return keyHolder.key!!.toInt()
    }

    fun delete(eventId: Int, idPlayer: Int) {
        val query =
            """
            delete from demo_player
            where id_player = :idPlayer
            and demo_event_id = :eventId
            """
        jdbcTemplate().update(
            query, mapOf(
                "idPlayer" to idPlayer,
                "eventId" to eventId
            )
        )
    }

    // privates

    private fun createCommonsParams(player: DemoPlayer): MapSqlParameterSource {
        val params = MapSqlParameterSource()
        params.addValue("eventId", player.demoEventId)
        params.addValue("name", player.name)
        params.addValue("description", player.description)
        return params
    }

}