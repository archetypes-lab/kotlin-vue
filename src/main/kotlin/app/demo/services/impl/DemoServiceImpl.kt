package app.demo.services.impl

import app.demo.dtos.DemoEventDto
import app.demo.models.DemoPlayer
import app.demo.repositories.DemoEventRepository
import app.demo.repositories.DemoPlayerRepository
import app.demo.services.DemoService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DemoServiceImpl(
    private val demoEventRepository: DemoEventRepository,
    private val demoPlayerRepository: DemoPlayerRepository
) : DemoService {

    @Transactional(readOnly = true)
    override fun getEventWithPlayers(idEvent: Int): DemoEventDto {
        val demoEvent = this.demoEventRepository.getById(idEvent)
        val players = this.demoPlayerRepository.findByEventId(idEvent)
        return DemoEventDto(demoEvent, players)
    }

    @Transactional(readOnly = false)
    override fun savePlayerInEvent(idEvent: Int, player: DemoPlayer) {
        player.demoEventId = idEvent
        if (player.idPlayer != null) {
            this.demoPlayerRepository.update(player)
        } else {
            this.demoPlayerRepository.insert(player)
        }
    }

    @Transactional(readOnly = false)
    override fun deletePlayer(idEvent: Int, idPlayer: Int) {
        this.deletePlayer(idEvent, idPlayer)
    }

}