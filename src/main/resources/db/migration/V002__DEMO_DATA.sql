insert into demo_event (id_event, name) values
    (1, 'Evento 1'),
    (2, 'Evento 2');

ALTER SEQUENCE demo_event_id_event_seq RESTART WITH 3;

insert into demo_player(id_player, demo_event_id, name, description) values
    (1, 1, 'Pedro Pablo', 'Jugador de Poker'),
    (2, 1, 'Alejandra Vasquez', 'Experta jugadora'),
    (3, 2, 'Juanito Perez', 'Un tipo cualquiera'),
    (4, 2, 'Mister Pipa', 'El entrenador del equipo');

ALTER SEQUENCE demo_player_id_player_seq RESTART WITH 5;

