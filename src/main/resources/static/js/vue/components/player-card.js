
function newPlayerCard() {
    return {
           props: ['player'],
           components: {
             'Card': primevue.card,
             'Button': primevue.button
           },
           template: `
             <Card>
                 <template #header>
                     {{ player.name }}
                 </template>
                 <template #content>
                     {{ player.description }}
                 </template>
                 <template #footer>
                    <Button icon="pi pi-check" label="Save" />
                    <Button icon="pi pi-times" label="Cancel" class="p-button-secondary" style="margin-left: .5em" />
                 </template>
             </Card>`
         }
}