package app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication 
class DemoApplication{

}

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}

@RestController
class MessageResource {

	@GetMapping("/hola")
	fun index(): List<Message> = listOf(
		Message("1", "Hello!"),
		Message("2", "Bonjour!"),
		Message("3", "Privet!"),
		Message("4", "Hola!")
	)
}

@Controller
class Inicio {

	@GetMapping("/")
	fun init(): String {
		return "index"
	}

}


data class Message(val id: String?, val text: String)
