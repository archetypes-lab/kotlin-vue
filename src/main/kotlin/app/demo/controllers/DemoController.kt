package app.demo.controllers

import app.demo.models.DemoPlayer
import app.demo.services.DemoService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import java.lang.IllegalArgumentException

@Controller
@RequestMapping("/demo")
class DemoController (
    private val demoService: DemoService
        ) {

    private val log = LoggerFactory.getLogger(javaClass)

    @GetMapping("/")
    fun inicioDemo(model: Model) : String {
        log.debug("-> inicioDemo")
        val eventWithPlayers = this.demoService.getEventWithPlayers(2)
        val event = eventWithPlayers.demoEvent
        val players = eventWithPlayers.demoPlayers
        model.addAttribute("eventName", event.name)
        model.addAttribute("players", players)
        return "demo/index"
    }

    @PostMapping("/save-player")
    fun saveDemoPlayer(
        @RequestParam eventId:Int,
        @RequestBody player: DemoPlayer, bindingResult: BindingResult) {
        log.debug("-> saveDemoPlayer. eventId {}, player: {}", eventId, player)
        if (bindingResult.hasErrors()) {
            throw IllegalArgumentException()
        }
        this.demoService.savePlayerInEvent(eventId, player)
    }

    @DeleteMapping("/delete-player")
    fun deleteDemoPlayer(@RequestParam eventId:Int, @RequestParam playerId: Int) {
        log.debug("-> deleteDemoPlayer eventId: {}, playerId: {}",
            eventId, playerId)
        this.demoService.deletePlayer(eventId, playerId)
    }

}