package app.framework.jdbc

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import javax.sql.DataSource

abstract class JdbcTemplateRepository(datasource: DataSource) {
    private val jdbcTemplate: NamedParameterJdbcTemplate

    init {
        jdbcTemplate = NamedParameterJdbcTemplate(datasource)
    }

    protected fun jdbcTemplate() : NamedParameterJdbcTemplate {
        return this.jdbcTemplate
    }

}