package app.demo.dtos

import app.demo.models.DemoEvent
import app.demo.models.DemoPlayer

data class DemoEventDto (
    val demoEvent: DemoEvent,
    val demoPlayers: List<DemoPlayer>
        )

