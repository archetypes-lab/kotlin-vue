package app.configs

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer

@Configuration
class FreemarkerConfig(
    @Value("\${app.env.productive}")
    private val envProductive: Boolean
) : BeanPostProcessor {



    override fun postProcessBeforeInitialization(bean: Any, beanName: String) : Any {
        if (bean is FreeMarkerConfigurer) {
            val commonsVars = hashMapOf<String, Any>()

            // enviroment
            commonsVars["PROD"] = envProductive

            // version js libs
            commonsVars["VUE_VERSION"] = "3.2.6"
            commonsVars["PRIMEVUE_VERSION"] = "3.6.4"
            commonsVars["PRIMEICONS_VERSION"] = "4.1.0"
            commonsVars["JQUERY_VERSION"] = "3.6.0"
            commonsVars["JQUERY_EASING_VERSION"] = "1.4.1"
            commonsVars["JQUERY_VALIDATION_VERSION"] = "1.19.1"
            commonsVars["BOOTSTRAP_VERSION"] = "5.1.0"
            commonsVars["AXIOS_VERSION"] = "0.19.2"
            commonsVars["LODASH_VERSION"] = "4.17.15"
            commonsVars["MOMENT_VERSION"] = "2.24.3"
            commonsVars["VEE_VALIDATE_VERSION"] = "3.3.0"
            commonsVars["FONT_AWESOME"] = "4.7.0"

            //json
            commonsVars["JSON"] = ObjectMapper()

            bean.setFreemarkerVariables(commonsVars)
        }
        return bean
    }

}